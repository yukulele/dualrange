export default /*html*/ `<div class="dualrange">
  <div class="dualrange-interval-bar">
    <div class="dualrange-interval"></div>
  </div>
  <div class="dualrange-handles">
    <div class="dualrange-handle dualrange-handle-1">
      <div class="dualrange-value-wrapper">
        <div class="dualrange-value"></div>
      </div>
    </div>
    <div class="dualrange-handle dualrange-handle-2">
      <div class="dualrange-value-wrapper">
        <div class="dualrange-value"></div>
      </div>
    </div>
  </div>
</div>`
