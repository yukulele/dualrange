import template from './template.js'

function round(a: number, step: number | null) {
  if (step) a = Math.round(a / step) * step
  return +a.toExponential(10)
}

export default class DualRange {
  public element: HTMLElement
  #values!: [number, number]
  #current: 0 | 1 | null = null
  #displayValues: [HTMLElement, HTMLElement]
  constructor(
    public inputs: [HTMLInputElement, HTMLInputElement],
    public min = 0,
    public max = 1,
    public step: number | null = null,
    public displayFunction: (value: number) => string = a => a.toString()
  ) {
    const templateElement = document.createElement('template')
    templateElement.innerHTML = template.trim()
    const element = templateElement.content.firstChild as HTMLElement
    this.element = element
    this.#displayValues = Array.from(
      element.querySelectorAll('.dualrange-value')
    ) as [HTMLElement, HTMLElement]
    for (const method of ['mousedown', 'mouseup', 'mousemove'] as [
      'mousedown',
      'mouseup',
      'mousemove'
    ])
      this[method] = this[method].bind(this)
    this.element.addEventListener('mousedown', this.mousedown)
    this.element.addEventListener('dragstart', ev => ev.preventDefault())
    this.setValues(inputs[0].valueAsNumber, inputs[1].valueAsNumber)
  }

  setValues(v1: number = this.#values[0], v2: number = this.#values[1]) {
    this.#values = [v1, v2].map(
      v => (v - this.min) / (this.max - this.min)
    ) as [number, number]
    this.update()
  }
  update() {
    let values = this.#values.map(v =>
      round(v * (this.max - this.min) + this.min, this.step)
    )
    values.sort((a, b) => a - b)
    for (const i of [0, 1]) {
      const v = this.inputs[i].valueAsNumber
      this.inputs[i].valueAsNumber = values[i]
      if (values[i] !== v) this.inputs[i].dispatchEvent(new Event('input'))
      this.#displayValues[i].textContent = this.displayFunction(values[i])
    }
    values = values.map(v => (v - this.min) / (this.max - this.min))
    this.element.style.setProperty('--min', values[0].toString())
    this.element.style.setProperty('--max', values[1].toString())
  }
  closest(x: number) {
    const dist = this.#values.map(v => Math.abs(v - x))
    return dist[0] < dist[1] ? 0 : 1
  }

  getPos(ev: MouseEvent) {
    var rect = this.element.getBoundingClientRect()
    return Math.max(0, Math.min((ev.clientX - rect.left) / rect.width, 1))
  }

  mousedown(ev: MouseEvent) {
    window.addEventListener('mouseup', this.mouseup)
    window.addEventListener('mousemove', this.mousemove)
    document.documentElement.classList.add('dualrange-focus')
    const x = this.getPos(ev)
    this.#current = this.closest(x)
    this.mousemove(ev)
  }

  mouseup(ev: MouseEvent) {
    window.removeEventListener('mouseup', this.mouseup)
    window.removeEventListener('mousemove', this.mousemove)
    document.documentElement.classList.remove('dualrange-focus')
    for (const input of this.inputs) {
      input.dispatchEvent(new Event('change'))
    }
    this.#current = null
  }

  mousemove(ev: MouseEvent) {
    if (this.#current === null) return
    const x = this.getPos(ev)
    this.#values[this.#current] = x
    this.update()
  }
}
