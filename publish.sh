#!/bin/sh

mkdir -p public
npm run build
cp -r ./dist ./public/
cp -r ./example ./public/

echo "<script>location='example'</script>" > ./public/index.html