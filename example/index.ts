import DualRange from '../dist/DualRange.js'

window.addEventListener('DOMContentLoaded', () => {
  const wrapper = document.getElementById('wrapper')!
  const i1 = document.getElementById('i1') as HTMLInputElement
  const i2 = document.getElementById('i2') as HTMLInputElement

  const dualRange = new DualRange(
    [i1, i2],
    -3.4,
    7.4,
    0.5,
    v => `${v.toFixed(2)} €`
  )

  wrapper.append(dualRange.element)
})
